import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SesizarilemelePage } from './sesizarilemele';

@NgModule({
  declarations: [
    SesizarilemelePage,
  ],
  imports: [
    IonicPageModule.forChild(SesizarilemelePage),
  ],
  exports: [
    SesizarilemelePage
  ]
})
export class SesizarilemelePageModule {}
