import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {HttpService} from "../../providers/httpservice/httpservice";
import {AdaugasesizarePage} from "../adaugasesizare/adaugasesizare";
import {DetaliisesizarePage} from "../detaliisesizare/detaliisesizare";
import {AppCfg} from "../../app/app.config";


/**
 * Generated class for the SesizarilemelePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-sesizarilemele',
    templateUrl: 'sesizarilemele.html',
})
export class SesizarilemelePage {


    public apiResponse: any;
    public isDataAvailable:boolean = false;


    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private _httpservice: HttpService,
                private loadingCtrl: LoadingController,
    ) {

    }

    ngOnInit() {

        this._httpservice.request('/issues', {
            method: 'get',
        }).map((response: Response) => {
            this.apiResponse = response;
            this.isDataAvailable = true;

        }).subscribe();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SesizarilemelePage');
    }

    selectSection(item) {


        AppCfg.globalLoader = this.loadingCtrl.create({
            content: 'Te rog asteapta...'
        });
        AppCfg.globalLoader.present();

        this.navCtrl.push(DetaliisesizarePage,item);
    }

}
