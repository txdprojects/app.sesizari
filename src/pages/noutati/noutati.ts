import {Component} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {Http} from "@angular/http";
import {InAppBrowser, InAppBrowserOptions} from '@ionic-native/in-app-browser';
import {HttpService} from "../../providers/httpservice/httpservice";
import {DetaliisesizarePage} from "../detaliisesizare/detaliisesizare";
import {DetaliinoutatiPage} from "../detaliinoutati/detaliinoutati";

/**
 * Generated class for the NoutatiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-noutati',
    templateUrl: 'noutati.html',
})
export class NoutatiPage {

    public news: any;
    options: InAppBrowserOptions = {
        location: 'yes',//Or 'no'
        hidden: 'no', //Or  'yes'
        clearcache: 'yes',
        clearsessioncache: 'yes',
        zoom: 'yes',//Android only ,shows browser zoom controls
        hardwareback: 'yes',
        mediaPlaybackRequiresUserAction: 'no',
        shouldPauseOnSuspend: 'no', //Android only
        closebuttoncaption: 'Close', //iOS only
        disallowoverscroll: 'no', //iOS only
        toolbar: 'yes', //iOS only
        enableViewportScale: 'no', //iOS only
        allowInlineMediaPlayback: 'no',//iOS only
        presentationstyle: 'pagesheet',//iOS only
        fullscreen: 'yes',//Windows only
    };

    constructor(
        public navCtrl: NavController,
        public http: Http,
        public theInAppBrowser: InAppBrowser,
        public _httpservice: HttpService) {

        this._httpservice.request('/users/news', {
            method: 'get',
        }).map((response: Response) => {
            this.news = response;
            console.log(this.news)
        }).subscribe();


    }

    selectSection(item) {
        this.navCtrl.push(DetaliinoutatiPage,item);
    }


}
