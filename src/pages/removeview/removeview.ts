import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the RemoveviewPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-removeview',
    templateUrl: 'removeview.html',
})
export class RemoveviewPage {

    title: string;
    url: any;

    constructor(

        public navCtrl: NavController,
        public navParams: NavParams,
        private sanitize: DomSanitizer) {
        this.url = this.sanitize.bypassSecurityTrustResourceUrl(this.navParams.data.url);

    }




    ionViewDidLoad() {

        console.log('ionViewDidLoad RemoveviewPage');
    }

}
