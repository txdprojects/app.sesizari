import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RemoveviewPage } from './removeview';

@NgModule({
  declarations: [
    RemoveviewPage,
  ],
  imports: [
    IonicPageModule.forChild(RemoveviewPage),
  ],
  exports: [
    RemoveviewPage
  ]
})
export class RemoveviewPageModule {}
