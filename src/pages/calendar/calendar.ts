import {ChangeDetectorRef, Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {HttpService} from "../../providers/httpservice/httpservice";
import {NgModule} from '@angular/core';
import {AppCfg} from "../../app/app.config";

/**
 * Generated class for the CalendarPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-calendar',
    templateUrl: 'calendar.html',
})
export class CalendarPage {

    public currentEvents: any;
    public eventList: any;
    public luni: any;

    scrollTopPosition: number = 0;

    maxMonth: any;
    minMonth: any;

    allowToGoBack: boolean;
    allowToGoForward: boolean;

    date: any;
    daysInThisMonth: any;
    daysInLastMonth: any;
    daysInNextMonth: any;
    monthNames: string[];
    currentMonth: any;
    currentMonthNumber: any;
    currentYear: any;
    currentDate: any;
    currentSelection: any;


    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public _httpservice: HttpService,
        private cd: ChangeDetectorRef,
    ) {



        this.monthNames = [
            'Ianuarie',
            'Februarie',
            'Martie',
            'Aprilie',
            'Mai',
            'Iunie',
            'Iulie',
            'August',
            'Septembrie',
            'Octombrie',
            'Noiembrie',
            'Decembrie'
        ];

        this.date = new Date();
        this.currentMonth = this.monthNames[this.date.getMonth()];
        this.currentMonthNumber = this.date.getMonth();
        this.currentYear = this.date.getFullYear();
        this.selectDate(this.date.getDate());
        this.getDaysOfMonth();
        this.allowToGoBack = false;
        this.allowToGoForward = true;

        this.maxMonth = this.date.getMonth() + AppCfg.calendarLimits.maxMonth;
        this.minMonth = this.date.getMonth() - AppCfg.calendarLimits.minMonth;

        //
        // this.currentEvents = [
        //     {
        //         start: new Date(),
        //         end:  new Date(),
        //         title: 'A 3 day event',
        //         color: '#ff00000',
        //     },
        //
        // ];
        // let now = new Date();
        // this._httpservice.externalRequest('https://www.primariatm.ro/evenimente/includes/api-events.php', {
        //     method: 'post',
        //     body: {'selectedDate': now.getFullYear() + '-' + now.getMonth() + '-' + now.getDay()}
        // }).map((response: Response) => {
        //     this.currentEvents = response;
        //     this.eventList = response;
        //     this.cd.detectChanges();
        // }).subscribe();

    }

    //
    // ngOnInit() {
    // }
    //
    // onMonthSelect(event) {
    //     this.cd.detectChanges();
    // }

    onDaySelect(event) {

        this._httpservice.externalRequest('https://www.primariatm.ro/evenimente/includes/api-events.php', {
            method: 'post',
            body: {'selectedDate': event.year + '-' + event.month + '-' + event.date}
        }).map((response: Response) => {
            this.eventList = response;
            this.cd.detectChanges();
            var elmnt = document.getElementById("#eventsContainer");
            elmnt.scrollTo(0,0);

        }).subscribe();
    }


    onScroll(event) {
        this.scrollTopPosition = event.target.scrollTop;
    }

    getMonday(d) {
        d = new Date(d);
        var day = d.getDay(),
            diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
        return new Date(d.setDate(diff));
    }

    getDaysOfMonth() {

        this.daysInThisMonth = new Array();
        this.daysInLastMonth = new Array();
        this.daysInNextMonth = new Array();

        this.currentMonth = this.monthNames[this.date.getMonth()];
        this.currentMonthNumber = this.date.getMonth();
        this.currentYear = this.date.getFullYear();

        if (this.date.getMonth() === new Date().getMonth()) {
            this.currentDate = new Date().getDate();
        } else {
            this.currentDate = 999;
        }

        let firstDayThisMonth = ((new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay() + 6) % 7)-1;

        console.log('--- firstDayThisMonth:',firstDayThisMonth);

        let prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
        for (let i = prevNumOfDays - firstDayThisMonth; i <= prevNumOfDays; i++) {
            this.daysInLastMonth.push(i);
        }

        let thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDate();
        for (let i = 0; i < thisNumOfDays; i++) {
            this.daysInThisMonth.push(i + 1);
        }

        let lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 0).getDay();
        let nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0).getDate();
        for (let i = 0; i < (6 - lastDayThisMonth); i++) {
            this.daysInNextMonth.push(i + 1);
        }
        let totalDays = this.daysInLastMonth.length + this.daysInThisMonth.length + this.daysInNextMonth.length;
        if (totalDays < 36) {
            for (let i = (7 - lastDayThisMonth); i < ((7 - lastDayThisMonth) + 7); i++) {
                this.daysInNextMonth.push(i);
            }
        }
    }


    selectDate(day) {
        this.currentSelection = day;
        this._httpservice.externalRequest('https://www.primariatm.ro/evenimente/includes/api-events.php', {
            method: 'post',
            body: {'selectedDate': this.currentYear + '-' + this.currentMonthNumber + '-' + day}
        }).map((response: Response) => {
            this.currentEvents = response;
            this.eventList = response;
            this.cd.detectChanges();
        }).subscribe();

    }

    goToLastMonth() {

        if(this.date.getMonth() > this.minMonth) {
            this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
            this.getDaysOfMonth();
            this.allowToGoBack = true;
            this.allowToGoForward = true;
        } else {
            this.allowToGoBack = false;
            this.allowToGoForward = true;
        }
    }

    goToNextMonth() {

        if( this.maxMonth >= this.date.getMonth()) {
            this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 1, 1);
            this.getDaysOfMonth();
            this.allowToGoForward = true;
            this.allowToGoBack = true;
        } else {
            this.allowToGoForward = false;
            this.allowToGoBack = true;
        }
    }


    ionViewDidLoad() {

    }

}
