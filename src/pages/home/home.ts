import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform, ViewController} from 'ionic-angular';
import {SesizariPage} from "../sesizari/sesizari";
import {AlertController} from 'ionic-angular';
import {RemoveviewPage} from "../removeview/removeview";
import {FormulareonlinePage} from "../formulareonline/formulareonline";
import {NoutatiPage} from "../noutati/noutati";
import {UtilePage} from "../utile/utile";
import {SondajePage} from "../sondaje/sondaje";
import {CalendarPage} from "../calendar/calendar";


import {InAppBrowser, InAppBrowserOptions} from "@ionic-native/in-app-browser";
import {ContactPage} from "../contact/contact";
import {LoginPage} from "../login/login";
import {Facebook} from "@ionic-native/facebook";
import {GooglePlus} from "@ionic-native/google-plus";
import {NativeStorage} from "@ionic-native/native-storage";
import {SesizarilemelePage} from "../sesizarilemele/sesizarilemele";
import {AppCfg} from "../../app/app.config";
import {Http} from "@angular/http";

/**
 * Generated class for the HomePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
})
export class HomePage implements OnInit {

    public unregisterBackButtonAction: any;
    public currentUser: any;

    params: any = {

        title: null,
        url: null,

    };

    weather: any = {
        temperature: null,
        icon: null,
        text: null,
        date: null,
        astronomy: null,
        forecast: [],
    };

    currency: any = {
        usd: null,
        eur: null,
    };

    options: InAppBrowserOptions = {
        location: 'yes',//Or 'no'
        hidden: 'no', //Or  'yes'
        clearcache: 'yes',
        clearsessioncache: 'yes',
        zoom: 'yes',//Android only ,shows browser zoom controls
        hardwareback: 'yes',
        mediaPlaybackRequiresUserAction: 'no',
        shouldPauseOnSuspend: 'no', //Android only
        closebuttoncaption: 'Close', //iOS only
        disallowoverscroll: 'no', //iOS only
        toolbar: 'yes', //iOS only
        enableViewportScale: 'no', //iOS only
        allowInlineMediaPlayback: 'no',//iOS only
        presentationstyle: 'pagesheet',//iOS only
        fullscreen: 'yes',//Windows only
    };


    constructor(
        public platform: Platform,
        public viewCtrl: ViewController,
        public navCtrl: NavController,
        public navParams: NavParams,
        public theInAppBrowser: InAppBrowser,
        public fb: Facebook,
        public google: GooglePlus,
        public nativeStorage: NativeStorage,
        public httpClient: Http) {

        this.loadRemoteData();
        this.currentUser = AppCfg.currentUser;
    }


    ngOnInit()
    {
        this.initializeBackButtonCustomHandler();
        this.nativeStorage.getItem('user')
            .then(function (data) {
                AppCfg.currentUser = data;
            }, function (error) {
            });
    }

    goto(page: string, title?: string, url?: string): void {


        switch (page) {

            case "sesizari":

                this.navCtrl.push(SesizariPage);
                break;

            case "noutati":

                this.navCtrl.push(NoutatiPage);
                break;

            case "utile":

                this.navCtrl.push(UtilePage);
                break;

            case "sondaje":

                this.navCtrl.push(SondajePage);
                break;

            case "calendar":
                this.navCtrl.push(CalendarPage);
                break;

            case "contact":

                this.navCtrl.push(ContactPage);
                break;

            case "sesizarilemele":

                this.navCtrl.push(SesizarilemelePage);
                break;

            case "primarialive":

                this.theInAppBrowser.create('https://www.facebook.com/pg/tvprimariatm/videos/', '_blank');

                break;

            case "formulareonline":

                this.navCtrl.push(FormulareonlinePage);
                break;

            case "remoteview":

                this.params.title = title;
                this.params.url = url;

                this.navCtrl.push(RemoveviewPage, this.params);
                break;

        }
    }


    ionViewDidEnter() {

    }

    ionViewWillEnter() {
        this.viewCtrl.showBackButton(false);
        this.unregisterBackButtonAction && this.unregisterBackButtonAction();
    }

    public initializeBackButtonCustomHandler(): void {
        this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
            this.customHandleBackButton();
        }, 10);
    }

    private customHandleBackButton(): void {
        // do what you need to do here ...
    }

    loadRemoteData() {

        this.httpClient.get('https://query.yahooapis.com/v1/public/yql?u=c&q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text=%22Timisoara,Timis,Romania%22)%20and%20u=%27c%27&format=json').map(res => res.json()).subscribe(data => {
            console.log('response api: ', data.query.results.channel.item.condition.temp)
            this.weather.temperature = data.query.results.channel.item.condition.temp;
            this.weather.astronomy = data.query.results.channel.astronomy;
            this.weather.forecast = data.query.results.channel.item.forecast.slice(1, 6);
            this.weather.text = data.query.results.channel.item.condition.text;
            // let date = new Date;
            // this.weather.date = date.parse(data.query.results.channel.lastBuildDate);

        });
        this.httpClient.get('http://www.floatrates.com/daily/eur.json').map(res => res.json()).subscribe(data => {
            this.currency.eur = data.ron.rate;
        });
        this.httpClient.get('http://www.floatrates.com/daily/usd.json').map(res => res.json()).subscribe(data => {
            this.currency.usd = data.ron.rate;
        });

    }

    logout() {
        var nav = this.navCtrl;
        this.fb.logout();
        this.google.logout();
        AppCfg.currentUser = [];
        this.nativeStorage.remove('user');
        nav.push(LoginPage);
    }

}
