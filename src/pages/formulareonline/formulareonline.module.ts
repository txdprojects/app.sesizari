import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormulareonlinePage } from './formulareonline';

@NgModule({
  declarations: [
    FormulareonlinePage,
  ],
  imports: [
    IonicPageModule.forChild(FormulareonlinePage),
  ],
  exports: [
    FormulareonlinePage
  ]
})
export class FormulareonlinePageModule {}
