import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {RemoveviewPage} from "../removeview/removeview";
import {AppCfg} from "../../app/app.config";

/**
 * Generated class for the FormulareonlinePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-formulareonline',
    templateUrl: 'formulareonline.html',
})
export class FormulareonlinePage {


    params: any = {
        title: null,
        url: null,
    };
    formulare: any;


    constructor(

        public navCtrl: NavController,
        public navParams: NavParams) {

        this.formulare = AppCfg.formulare;

    }


    goto(page: string, title?: string, url?: string): void {


        switch (page) {

            case "remoteview":

                this.params.title = title;
                this.params.url = url;

                this.navCtrl.push(RemoveviewPage, this.params);
                break;

        }
    }


    ionViewDidLoad() {
        console.log('ionViewDidLoad FormulareonlinePage');
    }


}
