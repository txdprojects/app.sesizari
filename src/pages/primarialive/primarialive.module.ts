import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrimarialivePage } from './primarialive';

@NgModule({
  declarations: [
    PrimarialivePage,
  ],
  imports: [
    IonicPageModule.forChild(PrimarialivePage),
  ],
  exports: [
    PrimarialivePage
  ]
})
export class PrimarialivePageModule {}
