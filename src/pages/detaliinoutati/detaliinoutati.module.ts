import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetaliinoutatiPage } from './detaliinoutati';

@NgModule({
  declarations: [
    DetaliinoutatiPage,
  ],
  imports: [
    IonicPageModule.forChild(DetaliinoutatiPage),
  ],
  exports: [
    DetaliinoutatiPage
  ]
})
export class DetaliinoutatiPageModule {}
