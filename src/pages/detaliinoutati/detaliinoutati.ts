import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DetaliinoutatiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-detaliinoutati',
  templateUrl: 'detaliinoutati.html',
})
export class DetaliinoutatiPage {

  public item: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
      this.item = this.navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetaliinoutatiPage');
  }

}
