import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {AbstractControl, FormBuilder, ValidatorFn, Validators} from "@angular/forms";
import {NativeStorage} from "@ionic-native/native-storage";
import {HttpService} from "../../providers/httpservice/httpservice";
import {ToasterService} from "../../providers/toasterservice/toasterservice";
import {LoginPage} from "../login/login";

/**
 * Generated class for the RecoverpasswordPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-recoverpassword',
    templateUrl: 'recoverpassword.html',
})
export class RecoverpasswordPage {

    public recoverForm: any;
    public userData: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public alertCtrl: AlertController,
        public httpService: HttpService,
        public nativeStorage: NativeStorage,
        public toasterService: ToasterService,
    ) {

        this.recoverForm = this.formBuilder.group({
            email: ['', Validators.compose([Validators.email, Validators.required])],

        });

    }

    equalto(fieldName): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            let input = control.value;
            let isValid = control.root.value[fieldName] == input
            if (!isValid)
                return {'equalTo': {isValid}}
            else
                return null;
        };
    }

    validate(): boolean {

        if (this.recoverForm.valid) {
            return true;
        } else {
            return false;
        }

    }

    submit(values) {

        if (this.validate()) {
            this.httpService.request('/users/forget-password', {
                method: 'POST',
                body: values
            }, {action: 'notify'}).map((responseData) => {

                let response = responseData;


                this.toasterService.show(response);
                this.navCtrl.push(LoginPage);


            }).subscribe();
        }

    }

}
