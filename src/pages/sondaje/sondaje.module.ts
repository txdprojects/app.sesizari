import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SondajePage } from './sondaje';

@NgModule({
  declarations: [
    SondajePage,
  ],
  imports: [
    IonicPageModule.forChild(SondajePage),
  ],
  exports: [
    SondajePage
  ]
})
export class SondajePageModule {}
