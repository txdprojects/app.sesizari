import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdaugasesizarePage } from './adaugasesizare';

@NgModule({
  declarations: [
    AdaugasesizarePage,
  ],
  imports: [
    IonicPageModule.forChild(AdaugasesizarePage),
  ],
  exports: [
    AdaugasesizarePage
  ]
})
export class AdaugasesizarePageModule {}
