import {ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';
import {Camera, CameraOptions} from '@ionic-native/camera';
import {NativeStorage} from "@ionic-native/native-storage";

import {FormGroup, FormControl, Validators, FormArray} from '@angular/forms';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import {Sesizare} from "../../interfaces/sesizare.interface";
import {HttpService} from "../../providers/httpservice/httpservice";
import {AppCfg} from "../../app/app.config";
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";
import {HomePage} from "../home/home";
import {Observable} from "rxjs/Observable";

/**
 * Generated class for the AdaugasesizarePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

declare var google;


@IonicPage()
@Component({
  selector: 'page-adaugasesizare',
  templateUrl: 'adaugasesizare.html',
})
export class AdaugasesizarePage {

  @ViewChild('map') mapElement: ElementRef;

  public myForm: FormGroup;
  public map: any;
  public picture: any;
  public coords: any;
  public imageURI: any;

  public isOk: Boolean = false;
  public hasPicture: Boolean = false;

  public response: boolean = false;
  public geocoder: any;
  public imageFileName = [];

  public extraFields: any;

  public err;


  options: CameraOptions = {
    quality: 80,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    correctOrientation: true,

  }

  optionsGallery: CameraOptions = {
    quality: 80,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    correctOrientation: true,

  }

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public location: Geolocation,
              private camera: Camera,
              public nativeStorage: NativeStorage,
              private _httpservice: HttpService,
              private transfer: FileTransfer,
              public loadingCtrl: LoadingController,
              private cd: ChangeDetectorRef,
              public alertCtrl: AlertController,
  ) {

  }

  ngOnInit() {
    this.myForm = new FormGroup({
      category: new FormControl('', [<any>Validators.required, <any>Validators.minLength(5)]),
      description: new FormControl(),
      address: new FormControl(),
      title: new FormControl(),
      image: new FormArray([]),
      longitude: new FormControl(),
      latitude: new FormControl(),
      extraField: new FormControl(),
    });
    this.loadMap();

    this.extraFields = this.navParams.data.extraFields;

    // console.log(JSON.stringify(this.navParams.data));

  }

  get images() {
    return this.myForm.get('image') as FormArray;
  }


  ionViewDidLoad() {

    this.myForm.controls['category'].setValue(this.navParams.data.category_id);
    this.myForm.controls['title'].setValue(this.navParams.data.type);

  }


  setIsOk(value: any) {
    this.isOk = value;
  }

  loadPicture() {

    this.camera.getPicture(this.options).then((imageData) => {

      // imageData is either a base64 encoded string or a file URI
      // If it's base64:

      this.imageURI = imageData;
      this.uploadFile();

    }, (err) => {
    });
  }

  loadGallery() {

    if (this.imageFileName.length === 5) {
      let alert = this.alertCtrl.create({
        title: 'Eroare',
        subTitle: 'Nu poti adauga mai mult de 5 imagini!',
        buttons: ['Bine...']
      });
      alert.present();
    }

    this.camera.getPicture(this.optionsGallery).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:

      this.imageURI = imageData;
      this.uploadFile();

    }, (err) => {
    });
  }


  loadMap() {

    this.location.getCurrentPosition().then((position) => {

      this.coords = position.coords;
      this.myForm.controls['address'].setValue(position.coords.longitude + ',' + position.coords.latitude);
      this.myForm.controls['longitude'].setValue(position.coords.longitude);
      this.myForm.controls['latitude'].setValue(position.coords.latitude);
      //
      // this.getCityName().map((response: Response) => {
      //     this.setIsOk(response);
      // }).subscribe();

      this.updatePosition(position);

    }, (error) => {

      let position = new google.maps.LatLng('45.7536649', '21.2258985');
      let mapOptions = {
        center: position,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      };
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
      let dogwalkMarker = new google.maps.Marker({position: position, title: 'Pozitia Curenta', draggable: true});
      dogwalkMarker.setMap(this.map);
      dogwalkMarker.addListener('dragend', () => {
        this.dragElement(dogwalkMarker);
      });

      this.myForm.controls['address'].setValue('45.7536649', '21.2258985');
      this.myForm.controls['longitude'].setValue('45.7536649');
      this.myForm.controls['latitude'].setValue('21.2258985');

    });

  }

  updatePosition(position) {
    let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    let dogwalkMarker = new google.maps.Marker({position: latLng, title: 'Pozitia Curenta', draggable: true});
    dogwalkMarker.setMap(this.map);
    dogwalkMarker.addListener('dragend', () => {
      this.dragElement(dogwalkMarker);
    });
    // this.getCityName().map((response: Response) => {
    //     this.setIsOk(response);
    //     this.updatePosition(latLng);
    // }).subscribe();

    this.myForm.controls['address'].setValue(position.coords.longitude + ',' + position.coords.latitude);
    this.myForm.controls['longitude'].setValue(position.coords.longitude);
    this.myForm.controls['latitude'].setValue(position.coords.latitude);


  }

  dragElement(dogwalkMarker) {
    if (dogwalkMarker.getPosition().lat()) {
      let newCenter = new google.maps.LatLng(dogwalkMarker.getPosition().lat(), dogwalkMarker.getPosition().lng());
      this.coords = newCenter;

      this.myForm.controls['address'].setValue(dogwalkMarker.getPosition().lat() + ',' + dogwalkMarker.getPosition().lng());
      this.myForm.controls['longitude'].setValue(dogwalkMarker.getPosition().lng());
      this.myForm.controls['latitude'].setValue(dogwalkMarker.getPosition().lat());

      //
      // this.getCityName(newCenter).map((response: Response) => {
      //     this.setIsOk(response);
      //     this.updatePosition(newCenter);
      // }).subscribe();

      console.log('update center to: ' + this.coords);
      dogwalkMarker.setMap(null);
      dogwalkMarker = new google.maps.Marker({position: newCenter, title: 'Pozitia Curenta', draggable: true,});
      dogwalkMarker.setMap(this.map);
      dogwalkMarker.addListener('dragend', () => {
        this.dragElement(dogwalkMarker);
      });
    }

  }


  // We moved to backend validation
  // Google was not providing the accurate city name
  // getCityName(center?: any) {
  //
  //     return Observable.create((observer) => {
  //
  //         let geocoder = new google.maps.Geocoder();
  //         let latLng: any;
  //
  //         if(center) {
  //             latLng = center;
  //         } else {
  //             latLng = new google.maps.LatLng(this.coords.latitude, this.coords.longitude);
  //         }
  //
  //         geocoder.geocode({'latLng': latLng}, function (results, status) {
  //
  //             if (status == google.maps.GeocoderStatus.OK) {
  //                 if (results[1]) {
  //                     var city = null;
  //                     for (var i = 0; i < results[0].address_components.length; i++) {
  //                         for (var b = 0; b < results[0].address_components[i].types.length; b++) {
  //                             if (results[0].address_components[i].types[b] == "administrative_area_level_2") {
  //                                 city = results[0].address_components[i];
  //                             }
  //                         }
  //                     }
  //
  //                     if (city.long_name == "Municipiul Timișoara") {
  //
  //                         this.isOk = true;
  //                         console.log("inside timisoara");
  //                         observer.next(this.isOk);
  //
  //                     } else {
  //
  //                         this.isOk = false;
  //                         console.log("outside timisoara");
  //                         observer.next(this.isOk);
  //
  //                     }
  //                 }
  //             }
  //
  //         });
  //     });
  // }


  sendRequest(model: Sesizare, isValid: boolean) {

    this._httpservice.requestCode('/issues', {
      method: 'POST',
      body: this.myForm.value
    }, {action: 'notify'}).map((response: Response) => {
      if (response) {
        this.err = response;
        this.navCtrl.push(HomePage);
      }

    }, ((err) => {
      this.err = err;
    })).subscribe();

  }

  uploadFile() {

    AppCfg.globalLoader = this.loadingCtrl.create({
      content: 'Te rog asteapta'
    });
    AppCfg.globalLoader.present();

    const fileTransfer: FileTransferObject = this.transfer.create();

    let options: FileUploadOptions = {
      fileKey: 'upfile',
      fileName: 'upfile',
      chunkedMode: false,
      mimeType: "image/jpeg",
      headers: {}
    }

    console.log('--- prepare upload');

    fileTransfer.upload(this.imageURI, AppCfg.endpoint + '/issues/upload', options)
      .then((data) => {

        let response = JSON.parse(data.response);
        this.imageFileName.push(AppCfg.mainPoint + '/public/' + response.data.file);
        this.images.push(new FormControl(AppCfg.mainPoint + '/public/' + response.data.file));
        this.hasPicture = true;

        AppCfg.globalLoader.dismiss();

      }, (err) => {
        AppCfg.globalLoader.dismiss();
        this.err = {
          error: err,
          img: this.imageURI,
          endpoint: AppCfg.endpoint
        };
      });


    // fileTransfer.upload(this.imageURI, AppCfg.endpoint + '/issues/upload', options)
    //     .then((data) => {
    //         console.log(data+" Uploaded Successfully");
    //     }, (err) => {
    //         console.log(err);
    //     });

  }

}
