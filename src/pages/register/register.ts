import {Component} from '@angular/core';
import {AlertController, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {HomePage} from "../home/home";
import {HttpService} from "../../providers/httpservice/httpservice";
import {NativeStorage} from "@ionic-native/native-storage";
import {LoginPage} from "../login/login";

/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})
export class RegisterPage {

    public registerForm: any;
    public userData: any;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public alertCtrl: AlertController,
        public httpService: HttpService,
        public nativeStorage: NativeStorage,
        public loading: LoadingController,
    ) {

        this.registerForm = this.formBuilder.group({
            name: ['', Validators.compose([Validators.minLength(7), Validators.required])],
            email: ['', Validators.compose([Validators.email, Validators.required])],
            password: ['', Validators.compose([Validators.minLength(5), Validators.required])],
            passwordConfirm: ['', Validators.compose([Validators.minLength(5), Validators.required, this.equalto('password')])],
            provider: 'Self Register',
            userID: new Date(),

        });

    }

    equalto(fieldName): ValidatorFn {
        return (control: AbstractControl): {[key: string]: any} => {
            let input = control.value;
            let isValid=control.root.value[fieldName]==input
            if(!isValid)
                return { 'equalTo': {isValid} }
            else
                return null;
        };
    }

    validate(): boolean {

        if (this.registerForm.valid) {
            return true;
        } else {
            return false;
        }

    }

    submit(values) {

        if(this.validate()) {
            this.httpService.request('/users/register', {
                method: 'POST',
                body: values
            }, {action: 'notify'}).map((responseData) => {

                let response = responseData;

                if(response.code) {

                    // this.userData = {
                    //     name: response.first_name,
                    //     email: response.email,
                    //     picture: response.avatar,
                    //     authToken: response.access_token,
                    // };
                    //
                    // this.nativeStorage.setItem('user', this.userData)
                    //     .then(
                    //         () => this.navCtrl.push(HomePage),
                    //         error => console.error(' *** ERROR' +  JSON.stringify(error))
                    //     );
                    //
                    //
                    // let loading = this.loading.create({
                    //     content: 'Contul a fost creeat, va rugam verificati adresa de email!'
                    // });
                    // loading.present();
                    // setTimeout(function () {
                    //     loading.dismissAll();
                    // }, 5000);

                    let loading = this.loading.create({
                        content: response.messages
                    });
                    loading.present();
                    setTimeout(function () {
                        loading.dismissAll();
                    }, 5000);

                    this.navCtrl.push(HomePage);

                } else {

                    let loading = this.loading.create({
                        content: response.messages
                    });
                    loading.present();
                    setTimeout(function () {
                        loading.dismissAll();
                    }, 5000);
                }


            }).subscribe();
        }

    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad RegisterPage');
    }

}
