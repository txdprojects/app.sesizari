import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SesizariPage } from './sesizari';

@NgModule({
  declarations: [
    SesizariPage,
  ],
  imports: [
    IonicPageModule.forChild(SesizariPage),
  ],
  exports: [
    SesizariPage
  ]
})
export class SesizariPageModule {}
