import {Component,ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import { Content } from 'ionic-angular';
import {AdaugasesizarePage} from "../adaugasesizare/adaugasesizare";
import {HttpService} from "../../providers/httpservice/httpservice";



/**
 * Generated class for the SesizariPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-sesizari',
    templateUrl: 'sesizari.html',
})
export class SesizariPage {

    public sesizari: any;
    public isDataAvailable: boolean;
    private params: any = {

        type: null,

    };

    @ViewChild(Content) content: Content;


    constructor(public navCtrl: NavController, public navParams: NavParams,
                public _httpservice: HttpService) {
    }


    ngOnInit() {

        this._httpservice.request('/issues/categories', {
            method: 'get',
        }).map((response: Response) => {
            this.sesizari = response;
            this.isDataAvailable = true;
        }).subscribe();
    }

    toggleSection(i) {

        this.content.scrollToTop();
        let curr = 0;
        for(let thisSesizare of this.sesizari) {
            if(curr != i) { thisSesizare.open = false;}
            curr++;
        }
        this.sesizari[i].open = (this.sesizari[i].open ? false : true);

    }
    selectSection(type,category,id,extraFields) {


        this.params = {
            'type' : type,
            'category' : category,
            'category_id': id,
            'extraFields': extraFields,

        };
        this.navCtrl.push(AdaugasesizarePage,this.params);
        console.log('loading categoryid:' + id);

    }


    ionViewDidLoad() {
        console.log('ionViewDidLoad SesizariPage');
    }

}
