import { Component } from '@angular/core';
import { Facebook } from '@ionic-native/facebook';
import { NativeStorage } from '@ionic-native/native-storage';
import { AlertController, NavController } from 'ionic-angular';
import { GooglePlus } from "@ionic-native/google-plus";
import { AppCfg } from "../../app/app.config";
import { HomePage } from "../home/home";
import { HttpService } from "../../providers/httpservice/httpservice";
import { FormBuilder, FormControl, Validators } from "@angular/forms";
import { RegisterPage } from "../register/register";
import { RecoverpasswordPage } from "../recoverpassword/recoverpassword";

@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {


    displayName: any;
    email: any;
    familyName: any;
    givenName: any;
    userId: any;
    imageUrl: any;
    isLoggedIn: boolean = false;
    loginForm: any = new FormControl;

    userData: any;

    constructor(
        public navCtrl: NavController,
        public fb: Facebook,
        public googlePlus: GooglePlus,
        public nativeStorage: NativeStorage,
        private httpService: HttpService,
        public formBuilder: FormBuilder,
        public alertCtrl: AlertController,
    ) {

        this.fb.browserInit(AppCfg.FB_API_KEY, "v2.8");

        this.loginForm = this.formBuilder.group({
            email: ['', Validators.compose([Validators.email, Validators.required])],
            password: ['', Validators.compose([Validators.minLength(3), Validators.required])],
        });

    }

    validate(): boolean {

        if (this.loginForm.valid) {
            return true;
        }

        // figure out the error message
        let errorMsg = '';

        // validate each field
        let control = this.loginForm.controls['email'];
        if (!control.valid) {

            console.log(JSON.stringify(control.errors));


            if (control.errors['required']) {
                errorMsg = 'Adresa de email este obligatorie';
            } else if (control.errors['minlength']) {
                errorMsg = 'The email must have at least 5 characters';
            }


        }


        let alert = this.alertCtrl.create({
            title: 'Eroare',
            subTitle: errorMsg,
            buttons: ['Bine...']
        });
        alert.present();

        return false;
    }

    submit(values) {

        if (this.validate()) {
            this.httpService.request('/users/login', {
                method: 'POST',
                body: values
            }, { action: 'notify' }).map((responseData) => {


                let response = responseData;

                this.userData = {
                    name: response.name,
                    email: response.email,
                    picture: response.avatar,
                    authToken: response.access_token,
                };

                this.nativeStorage.setItem('user', this.userData)
                    .then(
                        () => this.navCtrl.push(HomePage),
                        error => console.error(JSON.stringify(error))
                    );

            }).subscribe();
        }

    }

    doGoogleLogin() {

        this.googlePlus.login({})
            .then(res => {
                console.log(res);
                this.displayName = res.displayName;
                this.email = res.email;
                this.familyName = res.familyName;
                this.givenName = res.givenName;
                this.userId = res.userId;
                this.imageUrl = res.imageUrl;

                this.isLoggedIn = true;
            })
            .catch(err => console.error(err));
    }

    doFbLogin() {
        let permissions = new Array<string>();
        let nav = this.navCtrl;

        //the permissions your facebook app needs from the user
        console.log("Login with Facebook ID:", AppCfg.FB_API_KEY);
        permissions = ["email", "public_profile", "user_friends"];

        this.fb.login(permissions)
            .then((response) => {
                let userId = response.authResponse.userID;
                let params = new Array<string>();

                //Getting name and gender properties
                this.fb.api("/me?fields=name,email", params)
                    .then((user) => {

                        this.userData = {
                            name: user.name,
                            email: user.email,
                            picture: "https://graph.facebook.com/" + userId + "/picture?type=large",
                            authToken: null,
                            userID: user.id,
                            provider: 'Facebook',
                        };


                        console.log(' -- facebook response: ', JSON.stringify(this.userData));

                        this.httpService.request('/users/login', {
                            method: 'POST',
                            body: this.userData
                        }, {}).map((responseData) => {


                            console.log(' -- response: ', JSON.stringify(responseData));

                            this.userData.authToken = responseData.access_token;
                            AppCfg.currentUser = this.userData;

                            this.nativeStorage.setItem('user', this.userData)
                                .then(
                                    () => nav.push(HomePage),
                                    error => console.error('Error storing item Facebook', error)
                                );

                        }).subscribe();


                    })
            }, (error) => {
                console.log('unable to login via Facebook', error);
            });
    }

    register() {
        var navOptions = {
            animation: 'md-transition'
        };
        this.navCtrl.push(RegisterPage, null, navOptions);
    }

    recover() {
        var navOptions = {
            animation: 'md-transition'
        };
        this.navCtrl.push(RecoverpasswordPage, null, navOptions);
    }
}
