import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetaliisesizarePage } from './detaliisesizare';

@NgModule({
  declarations: [
    DetaliisesizarePage,
  ],
  imports: [
    IonicPageModule.forChild(DetaliisesizarePage),
  ],
  exports: [
    DetaliisesizarePage
  ]
})
export class DetaliisesizarePageModule {}
