import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {AppCfg} from "../../app/app.config";

/**
 * Generated class for the DetaliisesizarePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-detaliisesizare',
  templateUrl: 'detaliisesizare.html',
})
export class DetaliisesizarePage {

  item: any;
  public loading: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController) {
    this.item = this.navParams.data;
    if (this.item.picture) {
      this.item.picture = this.item.picture;
    }
  }

  ionViewDidLoad() {
    AppCfg.globalLoader.dismiss();
    console.log('ionViewDidLoad DetaliisesizarePage');
  }

  checkMany(object) {
    return Array.isArray(object);
  }
}
