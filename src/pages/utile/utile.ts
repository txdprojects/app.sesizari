import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HttpService} from "../../providers/httpservice/httpservice";

/**
 * Generated class for the UtilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-utile',
  templateUrl: 'utile.html',
})
export class UtilePage {

  public utile;

  constructor(public navCtrl: NavController, public navParams: NavParams, public _httpservice: HttpService) {

      this._httpservice.request('/users/utile', {
          method: 'get',
      }).map((response: Response) => {
          this.utile = response;
      }).subscribe();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UtilePage');
  }

}
