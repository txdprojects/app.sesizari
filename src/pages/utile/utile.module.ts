import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UtilePage } from './utile';

@NgModule({
  declarations: [
    UtilePage,
  ],
  imports: [
    IonicPageModule.forChild(UtilePage),
  ],
  exports: [
    UtilePage
  ]
})
export class UtilePageModule {}
