export interface Sesizare {
    picture: string;
    userID: string;
    message: string;
    title: string;
    category: string;
    description: string;
    address?: string;
}