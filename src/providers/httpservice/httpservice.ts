import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import {ToasterService} from "../toasterservice/toasterservice";

import {NativeStorage} from "@ionic-native/native-storage";
import {AppCfg} from "../../app/app.config";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {LoadingController} from "ionic-angular";
import {HomePage} from "../../pages/home/home";

@Injectable()
export class HttpService {

    private _headers: any;
    public loading: any;
    public loadingShow: boolean = false;

    constructor(
        private _http: Http,
        public nativeStorage: NativeStorage,
        private _toasterService: ToasterService,
        public loadingCtrl: LoadingController,
    ) {
        this.setHeaders();
    }

    /**
     * Set HTTP Headers
     */
    setHeaders(): void {

        this._headers = new Headers();
        this._headers.append("Content-Type", "application/json");

        if (AppCfg.currentUser.authToken) {
            this._headers.append("Authorization", 'Bearer ' + AppCfg.currentUser.authToken);
        }

        AppCfg.LOADING = true;

    }

    /**
     * Request
     * @param url
     * @param options
     * @param extra
     * @returns {any}
     */
    request(url: string, options: any, extra?: any): Observable<any> {


        this.loading = this.loadingCtrl.create({
            content: 'Te rog asteapta...'
        });
        this.loading.present();
        this.loadingShow = true;

        this.setHeaders();

        // Append HTTP headers
        options.headers = this._headers;
        // Create an observable wrapper

        return Observable.create((observer) => {
            // Make the HTTP request

            this._http.request(AppCfg.endpoint + url, options)
            // Transform the response
                .map(response => response.json())
                // Subscribe to the observable
                .subscribe((responseData: any) => {

                    // If backend flag is set to success
                    if (extra) {
                        this.applyExtraOperation(extra, responseData);
                    }

                    if (responseData.response.code) {
                        // Continue with the data


                        observer.next(responseData.data);
                        this.loading.dismiss();
                        this.loadingShow = false;


                    } else {

                        observer.next(responseData.data);
                        this.loading.dismiss();
                        this.loadingShow = false;


                    }

                }, (error) => {

                    if (this.loadingShow) {
                        this.loading.dismiss();
                        this.loadingShow = false;
                    }

                    if (extra) {
                        this.applyExtraOperation(extra, error);
                    }

                });


        });

    }

    /**
     * Request
     * @param url
     * @param options
     * @param extra
     * @returns {any}
     */
    requestCode(url: string, options: any, extra?: any): Observable<any> {


        this.loading = this.loadingCtrl.create({
            content: 'Te rog asteapta...'
        });
        this.loading.present();
        this.loadingShow = true;

        this.setHeaders();

        // Append HTTP headers
        options.headers = this._headers;
        // Create an observable wrapper

        return Observable.create((observer) => {
            // Make the HTTP request

            this._http.request(AppCfg.endpoint + url, options)
            // Transform the response
                .map(response => response.json())
                // Subscribe to the observable
                .subscribe((responseData: any) => {

                    // If backend flag is set to success
                    if (extra) {
                        this.applyExtraOperation(extra, responseData);
                    }

                    if (responseData.response.code) {
                        // Continue with the data


                        observer.next(responseData.response.code);
                        this.loading.dismiss();
                        this.loadingShow = false;


                    } else {

                        observer.next(responseData.response.code);
                        this.loading.dismiss();
                        this.loadingShow = false;


                    }

                }, (error) => {

                    if (this.loadingShow) {
                        this.loading.dismiss();
                        this.loadingShow = false;
                    }

                    if (extra) {
                        this.applyExtraOperation(extra, error);
                    }

                });


        });

    }

    /**
     * Request
     * @param url
     * @param options
     * @param extra
     * @returns {any}
     */
    externalRequest(url: string, options: any, extra?: any): Observable<any> {


        if (!this.loadingShow) {
            this.loading = this.loadingCtrl.create({
                content: 'Te rog asteapta...'
            });
            this.loading.present();
            this.loadingShow = true;
        }

        this.setHeaders();

        // Append HTTP headers
        options.headers = this._headers;
        // Create an observable wrapper

        return Observable.create((observer) => {
            // Make the HTTP request

            this._http.request(url, options)
            // Transform the response
                .map(response => response.json())
                // Subscribe to the observable
                .subscribe((responseData: any) => {

                    // If backend flag is set to success
                    if (extra) {
                        this.applyExtraOperation(extra, responseData);
                    }
                    if (responseData.response.code) {
                        // Continue with the data

                        observer.next(responseData.data);
                        if (this.loadingShow) {
                            this.loading.dismiss();
                            this.loadingShow = false;
                        }

                    } else {

                        observer.next(responseData.data);
                        if (this.loadingShow) {
                            this.loading.dismiss();
                            this.loadingShow = false;
                        }

                    }

                }, (error) => {

                    if (this.loadingShow) {
                        this.loading.dismiss();
                        this.loadingShow = false;
                    }

                    if (extra) {
                        this.applyExtraOperation(extra, error);
                    }

                });


        });

    }

    /**
     * Apply Extra Operation
     * @param extra
     * @param responseData
     */
    applyExtraOperation(extra: any, responseData: any): void {
        // If extra operation is a function call
        if (typeof this[`${extra.action}Action`] === 'function') {
            this[`${extra.action}Action`](responseData);
        }
    }

    isObject(val) {
        return typeof val === 'object';
    }

    /**
     * Notify Action
     * @param responseData
     */
    notifyAction(responseData: any): void {

        // If there are messages
        if (responseData && responseData.messages) {

            if (this.isObject(responseData.messages)) {

                this._toasterService.show(responseData.messages);
                this.loading.dismiss();

            } else {

                this._toasterService.show(responseData.messages);
                this.loading.dismiss();

            }
        }
    }

}