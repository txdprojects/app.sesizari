import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {ToastController} from "ionic-angular";

@Injectable()
export class ToasterService {

    public toasterStatus: BehaviorSubject<Object> = new BehaviorSubject<Object>(null);

    constructor(private toastCtrl: ToastController) {

    }

    show(data): void {

            let toast = this.toastCtrl.create({
                message: data,
                duration: 3000,
                position: 'bottom'
            });

            toast.present();
    }

    hide(): void {
        this.toasterStatus.next(null);
    }

}