import {Component, ViewChild} from '@angular/core';
import {Platform, Nav} from 'ionic-angular';
import {NativeStorage} from '@ionic-native/native-storage';
import {SplashScreen} from "@ionic-native/splash-screen";
import {StatusBar} from "@ionic-native/status-bar";


import {HomePage} from "../pages/home/home";
import {Geolocation} from '@ionic-native/geolocation';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {AppCfg} from "./app.config";
import {LoginPage} from "../pages/login/login";
import {CalendarPage} from "../pages/calendar/calendar";

@Component({
    template: `<ion-nav [root]="rootPage"></ion-nav>`
})
export class MyApp {

    @ViewChild(Nav) nav: Nav;
    rootPage: any;
    userData: any = {
        'authToken': null,
        'email': null,
        'name': null,
        'picture': null,
        'provider': null,
        'userID': null,
    };

    constructor(
        platform: Platform,
        public nativeStorage: NativeStorage,
        public splashScreen: SplashScreen,
        public statusBar: StatusBar,
        public geolocation: Geolocation,
    ) {
        platform.ready().then(() => {

            let env = this;

            let debug = true; // debug mode to skip the login

            if(debug) {

                this.userData.authToken = "ae6be1ea9263bf35d63344575836ee02a7e9d28d";
                this.userData.email = "razvan.2dor@gmail.com";
                this.userData.name = "Razvan Tudor";
                this.userData.picture = "https://graph.facebook.com/481522862263080/picture?type=large";
                this.userData.provider = "Facebook";
                this.userData.userID = "481522862263080";
                AppCfg.currentUser = this.userData;


                env.splashScreen.hide();
                env.nav.push(HomePage);

            } else {

                this.nativeStorage.getItem('user')
                    .then(function (data) {
                        AppCfg.currentUser = data;
                        env.nav.push(HomePage);
                        env.splashScreen.hide();
                    }, function (error) {
                        env.nav.push(LoginPage);
                        env.splashScreen.hide();
                    });

                this.statusBar.styleDefault();
            }


        });
    }
}
