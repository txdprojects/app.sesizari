export const AppCfg: any = {

    LOADING: false,
    FB_API_KEY: '211174939471294',
    GL_API_KEY: '649461981000-6pvg1hm3vlomunf3ntv8blmjpc00f96d.apps.googleusercontent.com', // Android Only
    mainPoint: 'https://app.primariatm.ro/api/web',

    globalLoader: null,
    endpoint: '/api',
    // endpoint: 'https://app.primariatm.ro/api/web/v1',

    calendarLimits: {
        minMonth: 0,
        maxMonth: 2,
    },

    luni: {
        "01": "Ian",
        "02": "Feb",
        "03": "Mar",
        "04": "Apr",
        "05": "Mai",
        "06": "Iun",
        "07": "Iul",
        "08": "Aug",
        "09": "Sep",
        "10": "Oct",
        "11": "Noi",
        "12": "Dec",
    },

    currentUser: false,

    formulare: [

        {
            "title": 'Declaraţie de începere a lucrărilor',
            "url": 'https://www.primariatm.ro/cerere_incepere_lucrari_mobile.php'
        },
        {
            "title": 'Declaraţie de încheiere a lucrărilor',
            "url": 'https://www.primariatm.ro/cerere_incheiere_lucrari_mobile.php'
        },
        {
            "title": 'Cerere eliberare duplicat AC/CU',
            "url": 'https://www.primariatm.ro/cerere_eliberare_duplicate_ac_cu_mobile.php'
        },

        {
            "title": 'Cerere aprobarea de construcţii la imobile fără AC',
            "url": 'https://www.primariatm.ro/cerere_aprobarea_lucrarilor_constructii_mobile.php'
        },
        {
            "title": 'Cerere pentru scutire impozit (doar anul lucrării)',
            "url": 'https://www.primariatm.ro/cerere_scutire_impozit_mobile.php'
        },
        {
            "title": 'Plan situaţie / Încadrare în zonă / PUG/PUZ/PUD',
            "url": 'https://www.primariatm.ro/cerere_plan_situatie_PUZ_PUD_PUG_mobile.php'
        },
        {
            "title": 'Notificarea vânzărilor de lichidare',
            "url": 'https://www.primariatm.ro/cerere_notificare_vanzari_lichidare_mobile.php'
        },
        {
            "title": 'Notificarea vânzărilor de soldare',
            "url": 'https://www.primariatm.ro/cerere_notificare_vanzari_soldare_mobile.php'
        },
        {
            "title": 'Cerere Evenimente pe domeniul public',
            "url": 'https://www.primariatm.ro/evenimente',
        },
    ],


    sesizari: null,


};
