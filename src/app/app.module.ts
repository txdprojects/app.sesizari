import {NgModule} from '@angular/core';
import {IonicApp, IonicModule} from 'ionic-angular';
import {MyApp} from './app.component';
import {NativeStorage} from '@ionic-native/native-storage';
import {SplashScreen} from "@ionic-native/splash-screen";
import {StatusBar} from "@ionic-native/status-bar";
import {LoginPage} from '../pages/login/login';
import {UserPage} from '../pages/user/user';
import {BrowserModule} from '@angular/platform-browser';
import {GooglePlus} from "@ionic-native/google-plus";
import {HttpService} from "../providers/httpservice/httpservice";
import {ToasterService} from "../providers/toasterservice/toasterservice";
import {HttpModule} from "@angular/http";
import {InAppBrowser} from '@ionic-native/in-app-browser';

import {Geolocation} from '@ionic-native/geolocation';
import {Camera} from '@ionic-native/camera';
import {ContactPageModule} from "../pages/contact/contact.module";
import {PrimarialivePageModule} from "../pages/primarialive/primarialive.module";
import {SondajePageModule} from "../pages/sondaje/sondaje.module";
import {UtilePageModule} from "../pages/utile/utile.module";
import {FormulareonlinePageModule} from "../pages/formulareonline/formulareonline.module";
import {RemoveviewPageModule} from "../pages/removeview/removeview.module";
import {SesizariPageModule} from "../pages/sesizari/sesizari.module";
import {SesizarilemelePageModule} from "../pages/sesizarilemele/sesizarilemele.module";
import {AdaugasesizarePageModule} from "../pages/adaugasesizare/adaugasesizare.module";
import {NoutatiPageModule} from "../pages/noutati/noutati.module";
import {MainPipe} from "../pipes/safe/safe.module";
import {HomePageModule} from "../pages/home/home.module";
import {FileTransfer} from "@ionic-native/file-transfer";
import {File} from '@ionic-native/file';
import {Facebook} from "@ionic-native/facebook";
import {DetaliisesizarePageModule} from "../pages/detaliisesizare/detaliisesizare.module";
import {DetaliinoutatiPageModule} from "../pages/detaliinoutati/detaliinoutati.module";
import {RegisterPageModule} from "../pages/register/register.module";
import {RecoverpasswordPageModule} from "../pages/recoverpassword/recoverpassword.module";
import {CalendarPageModule} from "../pages/calendar/calendar.module";

@NgModule({
    declarations: [
        MyApp,
        LoginPage,
        UserPage,
    ],
    imports: [
        MainPipe,
        RecoverpasswordPageModule,
        RegisterPageModule,
        DetaliinoutatiPageModule,
        DetaliisesizarePageModule,
        AdaugasesizarePageModule,
        SesizarilemelePageModule,
        ContactPageModule,
        PrimarialivePageModule,
        SondajePageModule,
        UtilePageModule,
        NoutatiPageModule,
        FormulareonlinePageModule,
        RemoveviewPageModule,
        SesizariPageModule,
        HomePageModule,
        BrowserModule,
        HttpModule,
        CalendarPageModule,
        IonicModule.forRoot(MyApp),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        LoginPage,
    ],
    providers: [
        FileTransfer,
        File,
        InAppBrowser,
        StatusBar,
        SplashScreen,
        GooglePlus,
        NativeStorage,
        HttpService,
        ToasterService,
        Geolocation,
        Camera,
        Facebook,
    ]
})
export class AppModule {
}
